import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    const pi = 3.14;
    const weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri"];
    var dt = new Date();
	var txt = dt.toLocaleTimeString();

    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>My 1st React</h2>
        </div>
        <p className="App-intro">
		  <span >The value of pi is: {pi}</span>
		  <ul>Weekdays: <br/>
		      {weekdays.map(day => <li>{day}</li>)}
		  </ul>
		  <span >The time is {txt}</span>
        </p>
      </div>
    );
  }
}

export default App;
