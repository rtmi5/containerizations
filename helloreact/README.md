# docker-helloreact
This is a Docker image of a React hello-world program. 
 Start React experiments without installing Node JS

> [Topcoder Tutorial](https://www.topcoder.com/challenge-details/30058010/)

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/docker-helloreact/)
3. From the cloned directory, build and run


```console
$ docker build -t helloreact .
$ docker run -d --name run-hello -p 49160:3000 helloreact
$ docker exec -it run-hello /bin/ash
/home/alpine/Desktop/helloworld $ curl -i localhost:3000
```

## Google App Engine
1. Setup [GCP SDK](https://cloud.google.com/console)
2. Git clone the [src](https://github.com/patterns/docker-helloreact)
3. From the cloned directory, build and run

```console
$ docker build -t helloreact .
$ mkdir hi
$ docker run -it --rm -v $(pwd)/hi:/home/alpine/hi helloreact

/home/alpine/Desktop/helloworld $ cp -r src public \
yarn.lock package.json app.yaml ~/hi
/home/alpine/Desktop/helloworld $ exit

$ gcloud app deploy hi
$ gcloud app browse
```


