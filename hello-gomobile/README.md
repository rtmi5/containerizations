# docker-android
This is a Docker image to start Android app development
 in Golang with the Gomobile toolchain  

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [Dockerfile](https://github.com/patterns/docker-android/)
3. From the cloned directory, build and run


```console
$ docker build -t android .
$ docker run -it --rm android
```

## Credits
The original Dockerfile was from 
 [Bitrise](https://github.com/bitrise-docker/android-ndk)

## Notes
To change the base image to Debian Buster (openjdk-11-jdk)
 added JAXB and activation modules. Then modified the CLASSPATH
 in sdkmanager. This gets us a working command which we use to
 grab the alpha version (cmdline-tools;latest) from the Canary
 channel.

 Search for the proposal JEP-320 to find articles with information
 about the Java removal of the XML library. Also see SO mentioned 
 [issue opened in the Android 
 repo](https://issuetracker.google.com/issues/67495440#comment13)

 

