[![sii](https://snapcraft.io//sii/badge.svg)](https://snapcraft.io/sii)
# sii
ii IRC snap. A minimalist FIFO named pipes based IRC client by
 [Suckless](https://suckless.org)

The whole source is less than 1000 LOC!

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/sii)

## Quickstart

Use the docker snapcore/snapcraft container to build/test the snap.
 This is recommended by Snapcraft docs because the snap must be 
 compatible with Ubuntu 16.04

```
 $ cd /path/to/snap-definition
 $ docker run -ti --rm -v $PWD:/app snapcore/snapcraft
   # cd /app && SNAPCRAFT_BUILD_INFO=1 snapcraft
   # exit 
 $ sudo snap install sii_1.8.0_amd64.snap --dangerous
 $ sii
```


## Credits

[Suckless Tools](https://suckless.org)([LICENSE](https://git.suckless.org/ii/file/LICENSE.html))

[Snapcraft](https://docs.snapcraft.io/build-snaps/your-first-snap)

[Ubuntu Snap tutorial](https://tutorials.ubuntu.com/tutorial/create-your-first-snap)


