
# st
Make a static linked binary of the st: simple terminal from 
 [Suckless](https://suckless.org)

The build is done with the Alpine Linux base image. 
With the Makefile and config.mk modified to link the static libraries.


## Quickstart

1. Clone the repo
2. Compile inside the container
3. Copy the binary to your prefered path

```
 $ git clone git@gitlab.com:rtmi5/containerizations.git
 $ docker build -t stbuilder containerizations/st
 $ docker run -ti --rm -v $PWD:/build -w /build stbuilder cp /opt/st-0.8.4/st ./st-gruvbox
 $ sudo chown $USER ./st-gruvbox && mv ./st-gruvbox $HOME/.local/bin/
 $ st-gruvbox -v
```


## Credits

[Suckless Tools](https://suckless.org)([LICENSE](https://git.suckless.org/st/file/LICENSE.html))

[Cautionary about static linking](https://medium.com/@neunhoef/static-binaries-for-a-c-application-f7c76f8041cf)

[Xft dependencies](https://github.com/freedesktop/libXft)

[Linking static Brotli](https://github.com/google/brotli/issues/795)


