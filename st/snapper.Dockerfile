ARG ARCH=

FROM ${ARCH}ubuntu:bionic
RUN apt update && apt install -y snapcraft
COPY . /build
WORKDIR /build
RUN snapcraft
