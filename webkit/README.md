# docker-webkit
This is a Docker image for starting webkit experiments using
 go-webkit.

> [go-webkit](http://github.com/mattn/go-webkit/)

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [Dockerfile](https://github.com/patterns/docker-webkit/)
3. From the cloned directory, build


```console
$ docker build -t webkit .
$ docker run -it --rm -p 8088:5900 webkit
```

- Then launch your VNC viewer at vnc://127.0.0.1:8088
- Type the VNC_PASSWORD when prompted.
- From inside the container, it's shift-alt-enter to launch a terminal.
- Launch the example webview at /home/alpine/webview
- Send the window to 2nd desktop with shift-alt-2, alt-2


## Credits

Go-Webkit is by
 [Yasuhiro Matsumoto](http://github.com/mattn/go-webkit/)

