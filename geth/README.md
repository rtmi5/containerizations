# docker-geth
This is a Docker image for the Geth CLI tool of
 [Ethereum](https://github.com/ethereum/go-ethereum/)


## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/docker-geth/)
3. From the cloned directory, build and run


```console
$ docker build -t geth .
$ docker run -it --rm -v $(pwd):/home/alpine/geth geth --datadir /home/alpine/geth account list
```

## Credits
The original Dockerfile was from 
 [Ethereum](https://github.com/ethereum/go-ethereum/)

