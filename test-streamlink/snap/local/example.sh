#!/bin/sh


# check for player argument 

for i in "$@"
do
case $i in
    -p=*|--player=*)
    PLAYER="${i#*=}"
    shift # past argument=value
    ;;
    *)
          # unknown option
    ;;
esac
done

if [ -n "${PLAYER}" ]; then
  echo "The player argument requires special handling by the snap."
  echo "Either pipe stdout, or connect to LiveProxy."
  echo "For example, to pipe to VLC, call: "
  echo "      test-streamlink <URL> 720p --stdout | vlc -"
  echo ""
  echo "To use LiveProxy for visiting Bob Ross at 720p: "
  echo "      vlc http://127.0.0.1:53422/play/?url=twitch.tv/bobross&default-stream=720p"
  echo ""
fi

# mimic streamlink behavior of launching VLC
test-streamlink "$@" --stdout | vlc -
