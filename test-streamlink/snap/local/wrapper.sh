#!/bin/sh



# Run-once default config
SLCONF=$SNAP_USER_COMMON/streamlinkrc
SLSCRIPT=$SNAP_USER_COMMON/example.sh
if [ ! -f $SLCONF ]; then
  # Start with example config
  cp $SNAP/default-streamlinkrc $SLCONF
  cp $SNAP/default-script $SLSCRIPT

  echo "# location of custom plugins
#plugin-dirs=$SNAP_USER_COMMON/plugins" >> $SLCONF
  mkdir -p $SNAP_USER_COMMON/plugins

  echo "Example script and config copied to $SNAP_USER_COMMON"
fi


exec $SNAP/usr/bin/python3 $SNAP/bin/streamlink --config=${SLCONF} "$@"
