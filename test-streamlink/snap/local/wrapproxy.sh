#!/bin/sh


export PROXYHOST="$(snapctl get proxy-host)"
export PROXYPORT="$(snapctl get proxy-port)"

exec $SNAP/bin/liveproxy --host=$PROXYHOST --port=$PROXYPORT
