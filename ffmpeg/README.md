# f15g
This is a arm64 snap of ffmpeg.
 See [ffmpeg](https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu).
 Began with the Homebridge static binary for ffmpeg,
 but then needed the `--enable-libxcb` feature.


## Quickstart

1.
2.



## Credits

[Gitlab to docker](https://www.docker.com/blog/multi-arch-build-what-about-gitlab-ci/)

[Multi project pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html)

[ffmpeg snap](https://github.com/snapcrafters/ffmpeg)

ffmpeg for [Homebridge](https://github.com/homebridge/ffmpeg-for-homebridge)

Static builds by [John Van Sickle](https://johnvansickle.com/ffmpeg/)([LICENSE](http://www.gnu.org/licenses/gpl-3.0.en.html))

[Cross-compiler steps](https://trac.ffmpeg.org/wiki/CompilationGuide/RaspberryPi)

[ffmpeg repo](https://git.ffmpeg.org/ffmpeg.git)([LICENSE](https://git.ffmpeg.org/gitweb/ffmpeg.git/blob/HEAD:/LICENSE.md))


