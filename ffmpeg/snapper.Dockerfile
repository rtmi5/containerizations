ARG ARCH=

FROM ${ARCH}ubuntu:bionic
RUN apt update && apt install -y snapcraft
COPY ./snap /build/snap
WORKDIR /build
RUN snapcraft
