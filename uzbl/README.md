# docker-uzbl
This is a Docker image for Uzbl 

> [www.uzbl.org](https://www.uzbl.org/)

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/docker-uzbl/)
3. From the cloned directory, build
4. Add preferences, run

You can assign the VNC password for the image:

```console
$ docker build --build-arg VNC_PASSWORD=abc123 -t uzbl .

```

For my preferences, I use Chinese fonts with this derived Dockerfile:

```docker
FROM uzbl
RUN apt-get update && apt-get install -y \
        ttf-ubuntu-font-family fonts-wqy-microhei \
        language-pack-zh-hant language-pack-gnome-zh-hant 
```


```console
$ docker build -t my-uzbl .
$ docker run -it --rm -p 8088:5900 my-uzbl
```

Then launch your VNC viewer at vnc://127.0.0.1:8088
