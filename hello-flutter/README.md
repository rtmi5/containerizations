# hello-flutter
This is a Docker image for the code-lab starter web app
 in Flutter 

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [repo](https://github.com/patterns/hello-flutter/)
3. From the cloned directory, build and run


```console
$ cd hello-flutter
$ docker build -f Dockerfile-base -t patterns/flutter:base .
$ docker build -f Dockerfile-web -t flw .
$ docker run -it --rm -v $PWD:/home/dartu/src -w /home/dartu/src flw create signin_example
$ docker run -it --rm -v $PWD:/home/dartu/src -w /home/dartu/src/signin_example flw build web

```

The build output is placed in the `build/web` directory

## Credits
Flutter web app
    [code lab](https://flutter.dev/docs/get-started/codelab-web)

Flutter and the Command Line
 by [Gonçalo Palma](https://link.medium.com/YEUdH07zQ1)
