# docker-btcvanity
This is a Docker image for Bitcoin vanity addresses. 
 The logic is a translation of Example 4-8 as written in
 [Mastering Bitcoin](https://www.bitcoinbook.info/).
 The example used C++ so
 here we made a Golang port as an exercise, and experiment
 with the BTC Suite packages.

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/docker-btcvanity/)
3. From the cloned directory, build and run

To see a random address like 1FUN*

```console
$ docker build -t vanity cmd
$ docker run -it --rm -e VANITY_PAT=1FUN vanity
```

