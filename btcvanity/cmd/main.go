/* See Mastering Bitcoin, Example 4-8
 */
package main

import (
	"flag"
	"fmt"
	"strings"

	"github.com/btcsuite/btcd/btcec"
	"github.com/btcsuite/btcd/chaincfg"
	"github.com/btcsuite/btcutil"
)

var testnet = flag.Bool("testnet", false, "operate on the testnet Bitcoin network")
var pattern = flag.String("pattern", "1KID", "The string we are searching")

// By default (without -testnet), use mainnet.
var chainParams = &chaincfg.MainNetParams

var prefix string

func main() {
	flag.Parse()
	if len(*pattern) > 6 {
		fmt.Println("Quitting, this pattern would take too much time.\n")
		return
	}
	if len(*pattern) > 4 {
		fmt.Println("This pattern could take awhile, please wait.\n")
		//TODO need timeout, because the wait feels like forever some times
	}
	prefix = strings.ToLower(*pattern)

	// Modify active network parameters if operating on testnet.
	if *testnet {
		chainParams = &chaincfg.TestNet3Params
	}

	var numAttempts int64 = 0
	var found string = ""
	for {
		numAttempts++

		privKey, err := btcec.NewPrivateKey(btcec.S256())
		if err != nil {
			fmt.Println("Failed to create private key!")
			panic(err)
		}

		addrPubKey, err := btcutil.NewAddressPubKey(
			privKey.PubKey().SerializeUncompressed(), chainParams)
		if err != nil {
			fmt.Println("Failed to calculate public key!")
			panic(err)
		}

		rcvAddr := addrPubKey.AddressPubKeyHash().EncodeAddress()
		if matchFound(rcvAddr) {
			found = rcvAddr
			break
		}
	}

	fmt.Printf("Vanity addr found: %s\nOnly took %d attempts.\n",
		found, numAttempts)
}

// Case-insensitive otherwise search performance suffers
func matchFound(address string) bool {
	// compare search pattern to the left-most substr
	lower := strings.ToLower(address)
	return strings.HasPrefix(lower, prefix)
}
