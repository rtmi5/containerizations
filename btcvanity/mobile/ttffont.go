// +build darwin linux

package main

import (
	"io/ioutil"
	"log"

	"github.com/golang/freetype/truetype"
	"golang.org/x/mobile/asset"
	mfont "golang.org/x/mobile/exp/font"
)

func LoadBodyFont() (*truetype.Font, error) {
	return LoadTTF("luximr.ttf")
}
func LoadBackgroundFont() (*truetype.Font, error) {
	return LoadTTF("plexifont.ttf")
}
func LoadTTF(assetfile string) (font *truetype.Font, err error) {
	file, err := asset.Open(assetfile)
	if err != nil {
		log.Printf("error opening font asset: %v\n", err)
		return loadFallbackFont()
	}
	defer file.Close()
	raw, err := ioutil.ReadAll(file)
	if err != nil {
		log.Printf("error reading font: %v\n", err)
		return loadFallbackFont()
	}
	font, err = truetype.Parse(raw)
	if err != nil {
		log.Printf("error parsing font: %v\n", err)
		return loadFallbackFont()
	}
	return font, nil
}

func loadFallbackFont() (font *truetype.Font, err error) {
	// Default font doesn't work on Darwin
	log.Println("using Monospace font")
	return truetype.Parse(mfont.Monospace())
}
