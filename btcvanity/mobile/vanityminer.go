// +build darwin linux

package main

import (
	"image"
	"image/color"
	"log"
	"strings"

	"github.com/golang/freetype/truetype"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/exp/gl/glutil"
	"golang.org/x/mobile/geom"
	"golang.org/x/mobile/gl"

	// todo refactor into qrcode-sprite file?
	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
	"image/draw"
)

type Miner struct {
	header        string
	body          string
	font          *truetype.Font
	bgfont        *truetype.Font
	cycle         uint8
	pool          chan string
	sample        chan string
	halt          bool
	prefix        string
	match         string
	ticksPerFrame int
}

func NewMiner(prefix string) *Miner {
	var g Miner
	g.reset(prefix)
	return &g
}

func (g *Miner) reset(prefix string) {
	var err error
	g.font, err = LoadBodyFont()
	if err != nil {
		log.Fatalf("error parsing font: %v", err)
	}
	g.bgfont, err = LoadBackgroundFont()
	if err != nil {
		log.Fatalf("error parsing font: %v", err)
	}

	if len(prefix) != 0 {
		g.prefix = strings.ToLower(prefix)
	} else {
		g.prefix = "1kid"
	}
	g.header = "Vanity: " + g.prefix
	g.pool = make(chan string)
	g.sample = make(chan string)
	go g.producer()
	go g.consumer()
}

// producer is to be run as goroutine and sends addresses in channel
func (g *Miner) producer() {
	for !g.halt {
		pub, err := RandomAddress()
		if err != nil {
			log.Fatal(err)
			break
		}
		g.pool <- pub
	}
	close(g.pool)
}

// consumer is to be run as goroutine rcv from the channel
func (g *Miner) consumer() {
	n := 0
	// Process items from the channel
	for x := range g.pool {
		if len(g.match) == 0 {

			// Check for termination condition
			if MatchFound(g.prefix, x) {
				g.match = x
				close(g.sample)
			} else {

				// Estimating 1000 ops per frame
				// Sample 0.001 addresses, send addr downstream for UI display
				n += 1
				if n == 1000 {
					n = 0
					g.sample <- x
				}
			}
		}
	}
}

func (g *Miner) Touch(down bool) {

}

// Update refreshes state data
func (g *Miner) Update() {
	if g.halt {
		return
	}

	pub, ok := <-g.sample

	if ok {
		// Sample 0.001 and display as values are eliminated
		g.body = pub
	} else {
		// Match detected
		g.halt = true
		g.body = g.match
		g.header = "MATCHED " + g.prefix
	}
}

func (g *Miner) Render(sz size.Event, glctx gl.Context, images *glutil.Images) {

	headerHeightPx := zoomHdrHt(sz)
	footerHeightPx := headerHeightPx
	fontsz := zoomFont(sz, 24)

	header := &TextSprite{
		text:            g.header,
		font:            g.font,
		widthPx:         sz.WidthPx,
		heightPx:        headerHeightPx,
		textColor:       image.Black,
		backgroundColor: image.NewUniform(color.RGBA{0x50, 0xF5, 0xD2, 0xFF}),
		fontSize:        fontsz,
		xPt:             0,
		yPt:             0,
		align:           Left,
	}
	header.Render(sz)

	g.orientBody(sz, headerHeightPx, footerHeightPx, fontsz)

	g.cycle += 1
	if g.cycle > 254 {
		g.cycle = 0
	}
	footer := &TextSprite{
		text:            "BTC",
		font:            g.bgfont,
		widthPx:         sz.WidthPx,
		heightPx:        footerHeightPx,
		textColor:       image.NewUniform(color.RGBA{0x00, 0x00, 0x00, g.cycle}),
		backgroundColor: image.NewUniform(color.RGBA{0x50, 0xF5, 0xD2, 0xFF}),
		fontSize:        zoomFont(sz, 48),
		xPt:             0,
		yPt:             PxToPt(sz, sz.HeightPx-footerHeightPx),
		align:           Right,
	}
	footer.Render(sz)
}

// PxToPt convert a size from pixels to points (based on screen PixelsPerPt)
func PxToPt(sz size.Event, sizePx int) geom.Pt {
	return geom.Pt(float32(sizePx) / sz.PixelsPerPt)
}

// zoomHdrHt calculates 1/6th ratio of screen as hdr/ftr height
func zoomHdrHt(sz size.Event) int {
	scale := sz.HeightPx / 6
	return int(scale)
}

// zoomFont calculates base/688 ratio of screen as font size
func zoomFont(sz size.Event, base int) float64 {
	scale := sz.HeightPx / 688
	return float64(scale * base)
}

// orientBody makes landscape single line and portrait QR code
func (g *Miner) orientBody(sz size.Event, hht int, fht int, fontsz float64) {
	bodyht := sz.HeightPx - hht - fht

	if sz.Orientation == size.OrientationPortrait && g.halt {

		err := g.drawBodyQRcode(sz, hht, g.body)
		if err != nil {
			panic(err)
		}

	} else {
		g.drawBodyRow(sz, hht, bodyht, g.body, fontsz)
	}
}

func (g *Miner) drawBodyRow(sz size.Event, startht int, rowht int, word string, fontsz float64) {

	row := &TextSprite{
		text:            word,
		font:            g.font,
		widthPx:         sz.WidthPx,
		heightPx:        rowht,
		textColor:       image.White,
		backgroundColor: image.NewUniform(color.RGBA{0x00, 0x00, 0x00, 0xFF}),
		fontSize:        fontsz,
		xPt:             0,
		yPt:             PxToPt(sz, startht),
	}
	row.Render(sz)
}

func (g *Miner) drawBodyQRcode(sz size.Event, startht int, word string) error {
	// Create QR code
	qrCode, err := qr.Encode(word, qr.M, qr.Auto)
	if err != nil {
		return err
	}
	qrCode, err = barcode.Scale(qrCode, 400, 400)
	if err != nil {
		return err
	}

	m := images.NewImage(qrCode.Bounds().Dx(), qrCode.Bounds().Dy())
	draw.Draw(m.RGBA, m.RGBA.Bounds(), qrCode, image.ZP, draw.Src)
	m.Upload()

	yoffset := (sz.HeightPx / 2) - (qrCode.Bounds().Dy() / 2)
	xoffset := (sz.WidthPx / 2) - (qrCode.Bounds().Dx() / 2)
	yPt := PxToPt(sz, yoffset)
	xPt := PxToPt(sz, xoffset)
	ptTopLeft := geom.Point{xPt, yPt}
	ptTopRight := geom.Point{xPt + sz.WidthPt, yPt}
	ptBottomLeft := geom.Point{xPt, yPt + sz.HeightPt}

	m.Draw(sz, ptTopLeft, ptTopRight, ptBottomLeft, sz.Bounds())
	m.Release()
	return nil
}
