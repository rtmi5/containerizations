# android-btcvanity
This is a Docker image to make a BTC Vanity address Android app
 in Golang with the Gomobile toolchain. The container
 helps save time by having the environment prep'd and
 ready with tools to build the APK.

![Image of Android app](https://patterns.github.io/tfx/images/android-btcvanity1.png)
![Image of Android app](https://patterns.github.io/tfx/images/android-btcvanity2.png)

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/docker-btcvanity/)
3. From the cloned directory, build and run


```console
$ docker build -t btcvanity mobile
$ docker run -it --rm -v $(pwd):/go/app/btcvanity btcvanity
# cp app.apk /go/app/btcvanity/btcvanity.apk
# exit
$ adb install btcvanity.apk
```

## Credits
The original Dockerfile started from 
 [Bitrise](https://github.com/bitrise-docker/android-ndk)

Luxi Mono is by [Bigelow & Holmes](https://www.fontsquirrel.com/fonts/luxi-mono)

Plexifont is by [Blue Vinyl Fonts](https://www.fontsquirrel.com/fonts/plexifont-bv)

Truetype font (TTF) rendering is from
 [Antoine Richard](https://github.com/antoine-richard/gomobile-text/)

QR Barcode is from
 [Florian Sundermann](https://github.com/boombuler/barcode/)

