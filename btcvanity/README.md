# btcvanity

This is a program for creating BTC vanity addresses.
 It follows the steps described chapter 4 of Mastering Bitcoin.

> [www.bitcoinbook.info](https://www.bitcoinbook.info/)

There's two versions:

- Command line (/cmd)
- Mobile app (/mobile)

