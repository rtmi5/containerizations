# docker-pybitcoin
This is a Docker image to experiment with the Python Bitcoin tools. 
 See Example 4-4 from
 [Mastering Bitcoin](https://www.bitcoinbook.info/)

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/docker-pybitcoin/)
3. From the cloned directory, build and run


```console
$ docker build -t pybitcoin .
$ docker run -it --rm pybitcoin
```

