# docker-surf
This is a Docker image for Surf webkit browser

> [surf.suckless.org](http://surf.suckless.org/)

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [Dockerfile](https://github.com/patterns/docker-surf/)
3. From the cloned directory, build


```console
$ docker build -t surf .
$ docker run -it --rm -p 8088:5900 surf
```

- Then launch your VNC viewer at vnc://127.0.0.1:8088
- Type the VNC_PASSWORD when prompted.
- From inside the container, it's shift-alt-enter to launch a terminal
- Type surf URL

```console
$ surf http://tools.suckless.org
```

## Credits

Surf is by 
 [Suckless Tools](http://surf.suckless.org/)

