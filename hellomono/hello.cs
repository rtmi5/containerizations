using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;

class Hello {
	static void Main() {
 //DEBUG xfer once
		if (!File.Exists("data.json")) {
			var w = new WebClient();
			w.DownloadFile("http://files.olo.com/pizzas.json", "data.json");
		}


		var s = File.ReadAllText("data.json");
		//DEBUG local file while testing repeatedly

		var opt = RegexOptions.Multiline | RegexOptions.IgnoreCase;
		var pat = @"""toppings""\:\s*\[([^\]]+)";
		var r = new Regex(pat, opt);
		var h = new Dictionary<string, int>();

		foreach (Match i in r.Matches(s)) {
			var key = i.Groups[1].ToString().Trim();
			if (!h.ContainsKey(key)) {
				h[key] = 1;
				continue;
			}
			var val = h[key];
			h[key] = val + 1;
		}

		// sort the total counts for ranking?
		var sorted = new List<List<string>>();
		foreach (var i in h) {
			if (sorted.Count <= i.Value) {
				for (int n = sorted.Count; n <= i.Value; n++) {
					sorted.Add(new List<string>());
				}
			}
			sorted[i.Value].Add(i.Key);
		}

		var sample = sorted.Count - 4000;
//DEBUG to see the top 5
// (pepperoni really causes a gap in the list, exposes poor choice in List)
// Stack seems like a good next optimization....


		// walk from highest to lowest
		for (int i = sorted.Count-1; i>sample; i--) {
			foreach (var j in sorted[i]) {
				var descr = String.Format("x{0} {1}", i, j);
				Console.WriteLine(descr);
			}
		}
	}
}
