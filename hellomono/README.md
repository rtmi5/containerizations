# docker-hellomono
This is a Docker image of a Mono hello-world program. 
 Run CLI C# code without doing a full install
 of VS / .NET redistributables

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/docker-hellomono/)
3. From the cloned directory, build and run


```console
$ docker build -t hellomono .
$ docker run -it --rm hellomono
```

