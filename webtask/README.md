# docker-webtask
Docker image for Webtask.io wt-cli command.
Docker lets us run it without installing Node. Docker
also allows execution on Linux/Win/MacOS

> [github.com/auth0/wt-cli](https://github.com/auth0/wt-cli)


# How to use this image

## With the source

1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/docker-webtask)
3. From the cloned directory, build and run


You can build and run the Docker image:

```console
$ docker build -t webtask .
$ docker run -it --rm -v $(pwd):/usr/src/app --entrypoint /bin/bash webtask
root@:/usr/src/app# wt init
```


## With the pre-built image

1. Install [Docker](https://docker.com/)
2. Run

```console
$ docker run -it --rm -v $(pwd):/usr/src/app --entrypoint /bin/bash patterns/webtask
root@:/usr/src/app# wt init
```


