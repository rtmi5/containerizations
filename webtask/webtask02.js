var google = require('googleapis')
var request = require('request')

const RESPONSE = {
	ERROR : {
		status : "error",
		message : "There was an error."
	},
	OK : {
		status : "ok",
		message : "Well done."
	}
};

module.exports = function(context, cb) {

		var soapi = 'https://api.stackexchange.com/2.2/questions?pagesize=1&sort=hot&site=movies.stackexchange.com'

		request({
					method: 'GET',
					uri: soapi,
					gzip: true
				},
				function (error, response, body) {
					if (response && response.statusCode == 200) {
						obj = JSON.parse(body);
						qi = obj.items[0];
						var debug = "id:" + qi.question_id + "; json:" + qi.title
						cb(null, {status: "ok", message: debug});
					} else {
						cb(null, {status: "error", message: error});
					}
		});
};


function test001(context, cb) {
//	var location = context.query.location;
		var urlshortener = google.urlshortener('v1');
		var params = { shortUrl: 'http://goo.gl/xKbRu3' };
		urlshortener.url.get(params, function (err, response) {
				if (err) {
						console.log('Encountered error', err);
						cb(null, RESPONSE.ERROR);
				} else {
						console.log('Long url is', response.longUrl);
						cb(null, RESPONSE.OK);
				}
		});
};

