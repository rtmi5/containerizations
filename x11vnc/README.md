# docker-x11vnc
This is an X11 VNC base image because I kept repeating
 these steps for multiple Docker images. It uses the
 window manager from Suckless Tools.

> [dwm.suckless.org](http://dwm.suckless.org/)

## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [src](https://github.com/patterns/docker-x11vnc/)
3. From the cloned directory, build

You can assign the VNC password for the image:

```console
$ docker build --build-arg VNC_PASSWORD=abc123 -t xvnc .
$ docker run -it --rm -p 8088:5900 xvnc
```

- Then launch your VNC viewer at vnc://127.0.0.1:8088
- Type the VNC_PASSWORD when prompted.
- From inside the container, it's shift-alt-enter to launch a terminal.


## Credits

The original dockerfile was from
 [Daniel Guerra](https://github.com/danielguerra69/alpine-vnc)

